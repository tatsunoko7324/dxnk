/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include "DxNuka.h"

#include <DxLib.h>
#include <map>
#include <memory>
#include <vector>
#include <nuklear.h>

std::map<struct nk_context*, std::unique_ptr<nk_user_font>> font_map;

class KeyState {
public:
    KeyState() {
        memset(key_, 0, sizeof(key_));
    }

    int get_pressed_frames(const int key) {
        return key_[key];
    }

    bool update_key_input() {
        char tmp_key[256];
        GetHitKeyStateAll(tmp_key);
        for (int i = 0; i < 256; ++i) {
            if (tmp_key[i] != 0) {
                key_[i]++;
            } else {
                key_[i] = 0;
            }
        }
        return true;
    }
private:
    char key_[256];
};

static KeyState key_state;

static struct nk_color interpolate(struct nk_color c1, struct nk_color c2, float fraction) {
    struct nk_color result;
    float r = c1.r + (c2.r - c1.r) * fraction;
    float g = c1.g + (c2.g - c1.g) * fraction;
    float b = c1.b + (c2.b - c1.b) * fraction;
    float a = c1.a + (c2.a - c1.a) * fraction;
    result.r = static_cast<nk_byte>(NK_CLAMP(0, r, 255));
    result.g = static_cast<nk_byte>(NK_CLAMP(0, g, 255));
    result.b = static_cast<nk_byte>(NK_CLAMP(0, b, 255));
    result.a = static_cast<nk_byte>(NK_CLAMP(0, a, 255));
    return result;
}

static COLOR_U8 get_vertex_color(struct nk_color top, struct nk_color left, struct nk_color bottom, struct nk_color right, int x, int y) {
    struct nk_color X1 = interpolate(left, top, static_cast<float>(x));
    struct nk_color X2 = interpolate(right, bottom, static_cast<float>(x));
    struct nk_color Y = interpolate(X1, X2, static_cast<float>(y));
    return GetColorU8(Y.r, Y.g, Y.b, Y.a);
}

int32_t dxnk_init(struct nk_context* nk_context,uint32_t font_size) {
    if(nk_context == nullptr) {
        return -1;
    }
    const auto dx_font = CreateFontToHandle(nullptr, font_size, -1, DX_FONTTYPE_NORMAL);
    auto nk_font = new struct nk_user_font();
    nk_font->userdata = nk_handle_id(dx_font);
    nk_font->height = static_cast<float>(font_size);
    nk_font->width = [](const nk_handle handle, float h, const char* str, const int len) -> float {
        return static_cast<float>(GetDrawStringWidthToHandle(str, len, handle.id));
    };
    font_map.insert(std::make_pair(nk_context, std::unique_ptr<struct nk_user_font>(nk_font)));
    return nk_init_default(nk_context, nk_font) ? 0 : -1;
}

void dxnk_free(struct nk_context* nk_context) {
    if(font_map.find(nk_context) != font_map.end()) {
        DeleteFontToHandle(font_map[nk_context]->userdata.id);
    }
    nk_free(nk_context);
}

void dxnk_update(struct nk_context* nk_context) {
    key_state.update_key_input();
    nk_input_begin(nk_context);

    {
        typedef struct {
            int dx_key;
            char c;
        } key_pair;
        std::vector<key_pair> key_list;
        key_list.push_back({ KEY_INPUT_0 ,'0' });
        key_list.push_back({ KEY_INPUT_1 ,'1' });
        key_list.push_back({ KEY_INPUT_2 ,'2' });
        key_list.push_back({ KEY_INPUT_3 ,'3' });
        key_list.push_back({ KEY_INPUT_4 ,'4' });
        key_list.push_back({ KEY_INPUT_5 ,'5' });
        key_list.push_back({ KEY_INPUT_6 ,'6' });
        key_list.push_back({ KEY_INPUT_7 ,'7' });
        key_list.push_back({ KEY_INPUT_8 ,'8' });
        key_list.push_back({ KEY_INPUT_9 ,'9' });
        key_list.push_back({ KEY_INPUT_0 ,'0' });
        key_list.push_back({ KEY_INPUT_A ,'a' });
        key_list.push_back({ KEY_INPUT_B ,'b' });
        key_list.push_back({ KEY_INPUT_C ,'c' });
        key_list.push_back({ KEY_INPUT_D ,'d' });
        key_list.push_back({ KEY_INPUT_E ,'e' });
        key_list.push_back({ KEY_INPUT_F ,'f' });
        key_list.push_back({ KEY_INPUT_G ,'g' });
        key_list.push_back({ KEY_INPUT_H ,'h' });
        key_list.push_back({ KEY_INPUT_I ,'i' });
        key_list.push_back({ KEY_INPUT_J ,'j' });
        key_list.push_back({ KEY_INPUT_K ,'k' });
        key_list.push_back({ KEY_INPUT_L ,'l' });
        key_list.push_back({ KEY_INPUT_M ,'m' });
        key_list.push_back({ KEY_INPUT_N ,'n' });
        key_list.push_back({ KEY_INPUT_O ,'o' });
        key_list.push_back({ KEY_INPUT_P ,'p' });
        key_list.push_back({ KEY_INPUT_Q ,'q' });
        key_list.push_back({ KEY_INPUT_R ,'r' });
        key_list.push_back({ KEY_INPUT_S ,'s' });
        key_list.push_back({ KEY_INPUT_T ,'t' });
        key_list.push_back({ KEY_INPUT_U ,'u' });
        key_list.push_back({ KEY_INPUT_V ,'v' });
        key_list.push_back({ KEY_INPUT_W ,'w' });
        key_list.push_back({ KEY_INPUT_X ,'x' });
        key_list.push_back({ KEY_INPUT_Y ,'y' });
        key_list.push_back({ KEY_INPUT_Z ,'z' });
        key_list.push_back({ KEY_INPUT_PERIOD , '.' });
        key_list.push_back({ KEY_INPUT_COMMA , ',' });
        key_list.push_back({ KEY_INPUT_SEMICOLON , ';' });
        key_list.push_back({ KEY_INPUT_COLON , ':' });

        for (auto c : key_list) {
            if (key_state.get_pressed_frames(c.dx_key) % 5 == 1) {
                nk_input_char(nk_context, c.c);
            }
        }
    }

    {
        typedef struct {
            int dx_key;
            nk_keys nk_key;
        } key_pair;
        std::vector<key_pair> key_list;
        key_list.push_back({ KEY_INPUT_DELETE,NK_KEY_DEL });
        key_list.push_back({ KEY_INPUT_RETURN,NK_KEY_ENTER });
        key_list.push_back({ KEY_INPUT_TAB,NK_KEY_TAB });
        key_list.push_back({ KEY_INPUT_BACK,NK_KEY_BACKSPACE });
        key_list.push_back({ KEY_INPUT_UP,NK_KEY_UP });
        key_list.push_back({ KEY_INPUT_DOWN,NK_KEY_DOWN });
        key_list.push_back({ KEY_INPUT_HOME,NK_KEY_TEXT_START });
        key_list.push_back({ KEY_INPUT_END,NK_KEY_TEXT_END });

        key_list.push_back({ KEY_INPUT_LSHIFT,NK_KEY_SHIFT });
        key_list.push_back({ KEY_INPUT_RSHIFT,NK_KEY_SHIFT });
        key_list.push_back({ KEY_INPUT_LCONTROL,NK_KEY_CTRL });
        key_list.push_back({ KEY_INPUT_RCONTROL,NK_KEY_CTRL });
        key_list.push_back({ KEY_INPUT_DELETE,NK_KEY_DEL });

        key_list.push_back({ KEY_INPUT_LEFT,NK_KEY_LEFT });
        key_list.push_back({ KEY_INPUT_RIGHT,NK_KEY_RIGHT });
        for (auto c : key_list) {
            nk_input_key(nk_context, c.nk_key, key_state.get_pressed_frames(c.dx_key) % 5 == 1);
        }
    }

    {
        auto mouse_x = 0;
        auto mouse_y = 0;
        const auto click = GetMouseInput();
        GetMousePoint(&mouse_x, &mouse_y);
        nk_input_motion(nk_context, mouse_x, mouse_y);
        nk_input_button(nk_context, NK_BUTTON_LEFT, mouse_x, mouse_y, (click & MOUSE_INPUT_LEFT) != 0);
        nk_input_button(nk_context, NK_BUTTON_RIGHT, mouse_x, mouse_y, (click & MOUSE_INPUT_RIGHT) != 0);
        nk_input_button(nk_context, NK_BUTTON_MIDDLE, mouse_x, mouse_y, (click & MOUSE_INPUT_MIDDLE) != 0);
        nk_input_scroll(nk_context, nk_vec2(GetMouseHWheelRotVolF(), GetMouseWheelRotVolF()));
    }

    nk_input_end(nk_context);
}

void dxnk_render(struct nk_context* nk_context) {
    const struct nk_command* cmd;

    nk_foreach(cmd, nk_context) {
        switch (cmd->type) {
        case NK_COMMAND_NOP:
            break;
        case NK_COMMAND_SCISSOR: {
            const auto s = reinterpret_cast<const struct nk_command_scissor*>(cmd);
            SetDrawArea(s->x, s->y, s->x + s->w, s->y + s->h);
        } break;
        case NK_COMMAND_LINE: {
            const auto l = reinterpret_cast<const struct nk_command_line *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, l->color.a);
            DrawLine(l->begin.x, l->begin.y, l->end.x, l->end.y, GetColor(l->color.r, l->color.g, l->color.b));
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_RECT: {
            const auto r = reinterpret_cast<const struct nk_command_rect *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, r->color.a);
            // DrawRoundRect(r->x, r->y, r->x + r->w, r->y + r->h, r->rounding, r->rounding, GetColor(r->color.r, r->color.g, r->color.b),FALSE);
            // DrawRoundRectはSetDrawAreaが効かないというバグがあるためDrawBoxで代用
            DrawBox(r->x, r->y, r->x + r->w, r->y + r->h, GetColor(r->color.r, r->color.g, r->color.b), FALSE);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_RECT_FILLED: {
            const auto r = reinterpret_cast<const struct nk_command_rect_filled *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, r->color.a);
            DrawRoundRect(r->x, r->y, r->x + r->w, r->y + r->h, r->rounding, r->rounding, GetColor(r->color.r, r->color.g, r->color.b), TRUE);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_CIRCLE: {
            const auto c = reinterpret_cast<const struct nk_command_circle *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, c->color.a);
            DrawOval(c->x + c->w / 2, c->y + c->h / 2, c->w / 2, c->h / 2, GetColor(c->color.r, c->color.g, c->color.b), FALSE, c->line_thickness);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_CIRCLE_FILLED: {
            const auto c = reinterpret_cast<const struct nk_command_circle_filled *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, c->color.a);
            DrawOval(c->x + c->w / 2, c->y + c->h / 2, c->w / 2, c->h / 2, GetColor(c->color.r, c->color.g, c->color.b), TRUE);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_TRIANGLE: {
            const auto t = reinterpret_cast<const struct nk_command_triangle*>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, t->color.a);
            DrawTriangle(t->a.x, t->a.y, t->b.x, t->b.y, t->c.x, t->c.y, GetColor(t->color.r, t->color.g, t->color.b), FALSE);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_TRIANGLE_FILLED: {
            const auto t = reinterpret_cast<const struct nk_command_triangle_filled *>(cmd);
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, t->color.a);
            DrawTriangle(t->a.x, t->a.y, t->b.x, t->b.y, t->c.x, t->c.y, GetColor(t->color.r, t->color.g, t->color.b), TRUE);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_POLYGON: {
            const auto p = reinterpret_cast<const struct nk_command_polygon*>(cmd);

            std::vector<LINEDATA> line_list;

            if (p->point_count == 0) {
                break;
            }

            const auto first = p->points[0];
            auto start = p->points[0];

            for (int i = 0; i < p->point_count; ++i) {
                LINEDATA l;
                l.x1 = start.x;
                l.y1 = start.y;
                l.x2 = p->points[i].x;
                l.y2 = p->points[i].y;
                l.color = GetColor(p->color.r, p->color.g, p->color.b);;
                l.pal = p->color.a;
                start = p->points[i];
                line_list.push_back(l);
            }

            if ((p->point_count % 2) != 0) {
                LINEDATA l;
                l.x1 = first.x;
                l.y1 = first.y;
                l.x2 = p->points[p->point_count - 1].x;
                l.y2 = p->points[p->point_count - 1].y;
                l.color = GetColor(p->color.r, p->color.g, p->color.b);;
                l.pal = p->color.a;
                line_list.push_back(l);
            }

            DrawLineSet(&line_list[0], line_list.size());
        } break;
        case NK_COMMAND_POLYGON_FILLED: {
            const auto p = reinterpret_cast<const struct nk_command_polygon_filled *>(cmd);
            std::vector<VERTEX2D> point_list;
            for (int i = 0; i < p->point_count; ++i) {
                VERTEX2D ver;
                ver.pos.x = p->points[i].x;
                ver.pos.y = p->points[i].y;
                ver.dif = GetColorU8(p->color.r, p->color.g, p->color.b, p->color.a);
                point_list.push_back(ver);
            }
            DrawPolygon2D(&point_list[0], point_list.size(), DX_NONE_GRAPH, TRUE);
        } break;
        case NK_COMMAND_POLYLINE: {
            const auto p = reinterpret_cast<const struct nk_command_polyline*>(cmd);
            std::vector<LINEDATA> line_list;

            if (p->point_count == 0) {
                break;
            }
            auto start = p->points[0];
            for (int i = 0; i < p->point_count; i += 2) {
                LINEDATA l;
                l.x1 = start.x;
                l.y1 = start.y;
                l.x2 = p->points[i].x;
                l.y2 = p->points[i].y;
                l.color = GetColor(p->color.r, p->color.g, p->color.b);;
                l.pal = p->color.a;
                start = p->points[i];
                line_list.push_back(l);
            }
            DrawLineSet(&line_list[0], line_list.size());
        } break;
        case NK_COMMAND_TEXT: {
            const auto t = reinterpret_cast<const struct nk_command_text*>(cmd);
            const auto font_handle = t->font->userdata.id;
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, t->foreground.a);
            DrawNStringToHandle(t->x, t->y, static_cast<const char*>(t->string), t->length, GetColor(t->foreground.r, t->foreground.g, t->foreground.b), font_handle);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_CURVE: {
            const auto q = reinterpret_cast<const struct nk_command_curve*>(cmd);
             // DXライブラリではベジェ曲線が描画できないので、直線で代用
            SetDrawBlendMode(DX_BLENDMODE_ALPHA, q->color.a);
            DrawLine(q->begin.x, q->begin.y, q->end.x, q->end.y, GetColor(q->color.r, q->color.g, q->color.b), q->line_thickness);
            SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
        } break;
        case NK_COMMAND_IMAGE: {
            const auto i = reinterpret_cast<const struct nk_command_image*>(cmd);
            DrawGraph(i->x, i->y, i->img.handle.id, TRUE);
        } break;
        case NK_COMMAND_RECT_MULTI_COLOR: {
            const auto c = reinterpret_cast<const struct nk_command_rect_multi_color*>(cmd);
            VERTEX2D v[4];
            ZeroMemory(v, sizeof(v));
            v[0].pos.x = c->x;
            v[0].pos.y = c->y;
            v[0].rhw = 1.0f;
            v[0].dif = get_vertex_color(c->top, c->left, c->bottom, c->right, 0, 0);
            v[1].pos.x = static_cast<float>(c->x + c->w);
            v[1].pos.y = c->y;
            v[1].rhw = 1.0f;
            v[1].dif = get_vertex_color(c->top, c->left, c->bottom, c->right, 1, 0);
            v[2].pos.x = c->x;
            v[2].pos.y = static_cast<float>(c->y + c->h);
            v[2].rhw = 1.0f;
            v[2].dif = get_vertex_color(c->top, c->left, c->bottom, c->right, 1, 1);
            v[3].pos.x = static_cast<float>(c->x + c->w);
            v[3].pos.y = static_cast<float>(c->y + c->h);
            v[3].rhw = 1.0f;
            v[3].dif = get_vertex_color(c->top, c->left, c->bottom, c->right, 0, 1);
            DrawPrimitive2D(v, 4, DX_PRIMTYPE_TRIANGLESTRIP, DX_NONE_GRAPH, TRUE);
        } break;
        case NK_COMMAND_ARC:
        case NK_COMMAND_ARC_FILLED:
        default:
            break;
        }
    }
    nk_clear(nk_context);
}

struct nk_image create_nk_image_from_handle(int handle) {
    struct nk_image image;
    image.handle = nk_handle_id(handle);
    GetGraphSize(handle, reinterpret_cast<int*>(&image.w), reinterpret_cast<int*>(&image.h));
    return image;
}

struct nk_image create_nk_image_from_file(const char* file) {
    struct nk_image image;
    image.handle = nk_handle_id(LoadGraph(file));
    GetGraphSize(image.handle.id, reinterpret_cast<int*>(&image.w), reinterpret_cast<int*>(&image.h));
    return image;
}
